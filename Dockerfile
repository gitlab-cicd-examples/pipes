# FROM apache/beam_python3.10_sdk:2.48.0
FROM python:3.10-slim

ARG TAG

RUN mkdir /packages
COPY ./source/flavia/dist/flavia-$TAG-py3-none-any.whl /packages/flavia-$TAG-py3-none-any.whl
COPY ./source/xixo/dist/xixo-$TAG-py3-none-any.whl /packages/xixo-$TAG-py3-none-any.whl

RUN pip install /packages/xixo-$TAG-py3-none-any.whl && pip install /packages/flavia-$TAG-py3-none-any.whl